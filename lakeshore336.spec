%define _prefix __auto__
%define gemopt opt
%define name epics_module-lakeshore336
%define version 3.14.12.2
%define release 2
%define repository gemdev
%define debug_package %{nil}
%define checkout  %(svnversion -cn %{sourcedir} | sed -r 's/.+://') 


Summary: %{name} Package, Gemini specific records for EPICS base
Name: %{name}
Version: %{version}
Release: %release.%(date +"%Y%m%d")svn%{checkout}%{?dist}
#Release: %{release}%{?dist}

License: GPL
Group: Gemini
BuildRoot: /var/tmp/%{name}-%{version}-root
Source0: %{name}-%{version}.tar.gz
BuildArch: %{arch}
Prefix: %{_prefix}
BuildRequires: epics-base-devel epics_module-asyn-devel epics_module-streamdevice-devel epics_module-synApps-std-devel
Requires: epics-base-devel epics_module-asyn-devel epics_module-streamdevice-devel epics_module-synApps-std-devel

%description
This is the module %{name}. EPICS Asyn module.

## If you want to have a devel-package to be generated uncomment the following:
%package devel
Summary: %{name}-devel Package
Group: Development/Gemini
Requires: %{name}
%description devel
This is the module %{name}-devel. EPICS Asyn module.

%prep
%setup -q -n %{name}

%build
make distclean uninstall
make

%install
%define __os_install_post %{nil}
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/lakeshore336
cp -r bin $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/lakeshore336/
#cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/lakeshore336/
#cp -r include $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/lakeshore336/
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/lakeshore336/
cp -r db $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/lakeshore336/

%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{gemopt}/epics/modules/lakeshore336
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{gemopt}/epics/modules/lakeshore336/bin
   #/%{_prefix}/%{gemopt}/epics/modules/lakeshore336/lib

%files devel
%defattr(-,root,root)
   #/%{_prefix}/%{gemopt}/epics/modules/lakeshore336/include
   /%{_prefix}/%{gemopt}/epics/modules/lakeshore336/dbd
   /%{_prefix}/%{gemopt}/epics/modules/lakeshore336/db

%changelog
* Wed Apr 15 2013 Matt Rippa <mrippa@gemini.edu> 3.14.12.2-0
- gemdev init
