%define _prefix __auto__
%define gemopt opt
%define name epics_module-asyn
%define version 3.14.12.2
%define release 4.20
%define repository gemdev
%define debug_package %{nil}

Summary: %{name} Package, Gemini specific records for EPICS base
Name: %{name}
Version: %{version}
Release: %{release}%{?dist}
License: GPL
Group: Gemini
BuildRoot: /var/tmp/%{name}-%{version}-root
Source0: %{name}-%{version}.tar.gz
BuildArch: %{arch}
Prefix: %{_prefix}
BuildRequires: epics-base-devel
Requires: epics-base-devel

%description
This is the module %{name}. EPICS Asyn module.

## If you want to have a devel-package to be generated uncomment the following:
%package devel
Summary: %{name}-devel Package
Group: Development/Gemini
Requires: %{name}
%description devel
This is the module %{name}-devel. EPICS Asyn module.

%prep
%setup -q -n %{name}

%build
make distclean uninstall
make

%install
%define __os_install_post %{nil}
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/asyn
cp -r bin $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/asyn/
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/asyn/
cp -r include $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/asyn/
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/asyn/
cp -r db $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/asyn/

%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{gemopt}/epics/modules/asyn
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{gemopt}/epics/modules/asyn/bin
   /%{_prefix}/%{gemopt}/epics/modules/asyn/lib

%files devel
%defattr(-,root,root)
   /%{_prefix}/%{gemopt}/epics/modules/asyn/include
   /%{_prefix}/%{gemopt}/epics/modules/asyn/dbd
   /%{_prefix}/%{gemopt}/epics/modules/asyn/db

%changelog
* Wed Mar 14 2012 Matt Rippa <mrippa@gemini.edu> 3.14.12.2-0
- gemdev init
