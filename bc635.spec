%define _prefix __auto__
%define gemopt opt
%define name epics_module-bc635
%define version 3.14.12.2
%define release 0
%define repository gemdev
%define debug_package %{nil}
%define host_arch %(uname)

#These global defines are added to prevent stripping
# symbols on vxWorks cross-compiled code
# Getting 'strip' to work is probably only needed for
# building a related debug sub-package
#
# But this prevents all the strip warnings
# mrippa 20120202
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

Summary: %{name} Bancomm package (OSL Version) for gemini timebus
Name: %{name}
Version: %{version}
Release: %{release}%{?dist}
License: EPICS Open License
Group: Applications/Engineering
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: %{arch}
Prefix: %{_prefix}
BuildRequires: epics-base-devel
Requires: epics-base-devel

%description
Bancomm time board  (OSL Version) EPICS support module

%prep
%setup -q -n %{name}

%build
make distclean uninstall
make

%install
%define __os_install_post %{nil}
export DONT_STRIP=1
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/bc635
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/bc635/
cp -r include $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/bc635/
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/bc635/

%postun
if [ "$1" = "0" ]; then
	rm -rf/%{_prefix}/%{gemopt}/epics/modules/bc635
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/%{_prefix}/%{gemopt}/epics/modules/bc635

%changelog
 * Fri May 8 2012 Mathew Rippa <mrippa@gemini.edu> 3.14.12.2-0
 - r3.14.12.2, rpmlint compliant
* Sat Feb 5 2011 Matthieu Bec <mbec@gemini.edu> x0x0x
- no devel
* Tue Feb 16 2010 Matthieu Bec <mbec@gemini.edu> 3.14.9-5
- set CAD defaults CTYP=20, PREC=5
* Tue Feb 11 2008 Felix Kraemer <fkraemer@gemini.edu> 2.0.11-1
- changes to be compliant with the EPICS build scheme
* Thu Dec 20 2007 Felix Kraemer <fkraemer@gemini.edu> 2.0.11-0
- initial release
