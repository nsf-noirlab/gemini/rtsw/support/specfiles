%define _prefix __auto__
%define gemopt opt
%define name epics_module-iocStats
%define version 3.14.12.2
%define release 1
%define repository gemdev
%define debug_package %{nil}
%define checkout  %(svnversion -cn %{sourcedir} | sed -r 's/.+://') 


Summary: %{name} Package, Gemini specific records for EPICS base
Name: %{name}
Version: %{version}
#Release: %{release}%{?dist}
Release: %release.%(date +"%Y%m%d")svn%{checkout}%{?dist}
License: GPL
Group: Gemini
BuildRoot: /var/tmp/%{name}-%{version}-root
Source0: %{name}-%{version}.tar.gz
BuildArch: %{arch}
Prefix: %{_prefix}
BuildRequires: epics-base-devel
Requires: epics-base-devel

%description
This is the module %{name}. EPICS Asyn module.

%prep
%setup -q -n %{name}

%build
make distclean uninstall
make

%install
%define __os_install_post %{nil}
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/iocStats
cp -r bin $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/iocStats/
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/iocStats/
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/iocStats/
cp -r db $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/iocStats/

%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{gemopt}/epics/modules/iocStats
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{gemopt}/epics/modules/iocStats/bin
   /%{_prefix}/%{gemopt}/epics/modules/iocStats/dbd
   /%{_prefix}/%{gemopt}/epics/modules/iocStats/lib
   /%{_prefix}/%{gemopt}/epics/modules/iocStats/db

%changelog
* Thu Jan 30 2014 Matt Rippa <mrippa@gemini.edu> 3.14.12.2-1
- gemdev init
