%define _prefix __auto__
%define gemopt opt
%define name epics_module-libts-compat
%define _prefix __auto__
%define version __auto__
%define release __auto__
%define repository gemini

%define debug_package %{nil}

Summary: backward compatible libts.
Name: %{name}
Version: %{version}
Release: %{release}%{dist}
License: EPICS Open License
Group: Gemini
Source0: %{name}-%{version}.tar.gz
BuildRoot: /var/tmp/%{name}-%{version}-root
BuildArch: %{arch}
Prefix: %{_prefix}
Requires: gemini-setup epics-base epics_extensions-configure
## Switch dependency checking off
# AutoReqProv: no

%description
This package provides a backward compatible libts for older extensions like 'cau' and 'opi'.

%package devel
Summary: backward compatible libts header for development.
Group: Development/Gemini
Requires: %{name} epics-base-devel
Requires: epics_extensions-configure-devel
%description devel
This package provides the header needed to compile against libts.

%prep
%setup -n %{name}

%build
gmake

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/libts-compat
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/libts-compat
cp -r include $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/libts-compat

%post
/sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/%{_prefix}/%{gemopt}/epics/modules/libts-compat/lib/*

%files devel
%defattr(-,root,root)
/%{_prefix}/%{gemopt}/epics/modules/libts-compat/include/*

%changelog
 * Mon Mar 31 2008 Matthieu Bec
 - Updated spec file for gemini-rpmtools
 * Thu Dec 6 2007 Felix Kraemer
 - Initial release
