%define _prefix __auto__
%define gemopt opt
%define name epics_module-geminiRec
%define version 3.14.12.2
%define release 0
%define repository gemdev
%define debug_package %{nil}

Summary: %{name} Package, Gemini specific records for EPICS base
Name: %{name}
Version: %{version}
Release: %{release}%{?dist}
License: OSL
Group: Gemini
BuildRoot: /var/tmp/%{name}-%{version}-root
Source0: %{name}-%{version}.tar.gz
BuildArch: %{arch}
Prefix: %{_prefix}
BuildRequires: epics-base-devel
Requires: epics-base-devel

%description
%{name} Gemini specific records for EPICS base.

%prep
%setup -q -n %{name}

%build
make distclean uninstall
make

%install
%define __os_install_post %{nil}
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/geminiRec
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/geminiRec/
cp -r include $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/geminiRec/
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/geminiRec/


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{gemopt}/epics/modules/geminiRec/lib
   /%{_prefix}/%{gemopt}/epics/modules/geminiRec/include
   /%{_prefix}/%{gemopt}/epics/modules/geminiRec/dbd

%changelog
* Wed Mar 14 2012 Matt Rippa <mrippa@gemini.edu> 3.14.12.2-0
- rpmlint complint
* Sat Feb 5 2011 Matthieu Bec <mbec@gemini.edu> 3.14.9-5
- no devel
* Tue Feb 16 2010 Matthieu Bec <mbec@gemini.edu> 3.14.9-5
- set CAD defaults CTYP=20, PREC=5
* Tue Feb 11 2008 Felix Kraemer <fkraemer@gemini.edu> 2.0.11-1
- changes to be compliant with the EPICS build scheme
* Thu Dec 20 2007 Felix Kraemer <fkraemer@gemini.edu> 2.0.11-0
- initial release
