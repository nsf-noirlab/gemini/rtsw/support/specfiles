%define _prefix __auto__
%define gemopt opt
%define name epics_module-edif
%define version 1.0
%define release 1
%define repository gemdev
%define debug_package %{nil}
%define homep3 /home/p3

Summary: Experimental Physics and Industrial Control System %{name} Extension Package
Name: %{name}
Version: %{version}
Release: %{release}%{?dist}
License: EPICS Open License
## Source:%{name}-%{version}.tar.gz
Group: Gemini
Source0: %{name}-%{version}.tar.gz
ExclusiveArch: %{arch}
Prefix: %{_prefix}
## You may specify dependencies here
Requires: gemini-setup epics-base epics_extensions-configure
BuildRequires: epics-base-devel epics_extensions-configure 
## Switch dependency checking off
# AutoReqProv: no

%description
EPICS is a set of Open Source software tools, libraries and applications developed collaboratively and used worldwide to create distributed soft real-time control systems for scientific instruments such as a particle accelerators, telescopes and other large scientific experiments.
This package provides the %{name} extension.

%package p3
Summary: Experimental Physics and Industrial Control System %{name} Extension Package
Group: Applications/Engineering
%description p3
EPICS is a set of Open Source software tools, libraries and applications developed collaboratively and used worldwide to create distributed soft real-time control systems for scientific instruments such as a particle accelerators, telescopes and other large scientific experiments.
This package provides the %{name} extension.

%prep
%setup -q -n %{name}

%build
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/extensions
cp -r bin $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/extensions
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/extensions
mkdir -p $RPM_BUILD_ROOT%{homep3}/wcs/lib
cp -r p3/epics $RPM_BUILD_ROOT%{homep3}
cp cad.rc $RPM_BUILD_ROOT%{homep3}/wcs/lib
rm -fr `find $RPM_BUILD_ROOT -name .svn`

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_prefix}/%{gemopt}/epics/extensions/bin
%{_prefix}/%{gemopt}/epics/extensions/lib

%files p3
%defattr(-,root,root)
%{homep3}/epics
%{homep3}/wcs/lib/cad.rc

%changelog
* Tue Mar 11 2012 Matt Rippa <mrippa@gemini.edu> 1.0-1
- rpmlint compliance
* Tue Mar 31 2009 Matthieu Bec
- Initial release
