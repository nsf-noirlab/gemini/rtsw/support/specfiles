%define _prefix __auto__
%define gemopt opt
%define name epics_module-capfast
%define version 0.1
%define release 3
%define repository gemdev
%define debug_package %{nil}
%define homep3 %{_prefix}/%{gemopt}/epics/p3
%define checkout %(svnversion -cn %{sourcedir} | sed -r 's/.+://') 


#These global defines are added to prevent stripping
# symbols on vxWorks cross-compiled code
# Getting 'strip' to work is probably only needed for
# building a related debug sub-package
#
# But this prevents all the strip warnings
# mrippa 20120202
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}


Summary: EPICS Edif libraries
Name: %{name}
Version: %{version}
#Release: %{release}%{?dist}%{repository}
Release: %release.%(date +"%Y%m%d")svn%{checkout}%{?dist}
Source0: %{name}-%{version}.tar.gz
License: GPLv3+
Group: Applications/Engineering
URL: http://source.gemini.edu/software/epics_module-capfast/branches/gem10-rc1.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: %{arch}
Prefix: %{_prefix}
#Requires(pre): /usr/sbin/useradd
## You may specify dependencies here
# BuildRequires:
## Switch dependency checking off
# AutoReqProv: no
%description
Core Capfast files

%package p3
Summary: Capfast p3 user
Group: Applications/Engineering
Requires: epics_module-capfast
%description p3
Includes Capfast executable files and the p3 user account. This is required for Capfast specific utilites to execute. Capfast license keys references are maintained in the p3 user home directory.

%prep
## Do some preparation stuff, e.g. unpacking the source with
%setup -q -n %{name}

%build
## Write build instructions here, e.g
# sh configure
# make

%install
export DONT_STRIP=1
rm -rf $RPM_BUILD_ROOT
mkdir -p  $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/capfast/bin/
mkdir -p  $RPM_BUILD_ROOT/%{_prefix}/etc/profile.d/
cp -r bin $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/capfast/
cp -r etc/capfast-env.csh $RPM_BUILD_ROOT/%{_prefix}/etc/profile.d/capfast-env.csh

mkdir -p $RPM_BUILD_ROOT/%{homep3}
cp -r p3/* $RPM_BUILD_ROOT/%{homep3}
cp -r p3/wcs/lib $RPM_BUILD_ROOT%{homep3}/wcs
#cp cad.rc $RPM_BUILD_ROOT%{homep3}/wcs/lib
rm -fr `find $RPM_BUILD_ROOT -name .svn`
chmod -R u+w $RPM_BUILD_ROOT/%{_prefix}


## if you want to do something after installation uncomment the following
## and list the actions to perform:
# %post
## actions, e.g. /sbin/ldconfig

## If you want to have a devel-package to be generated and do some
## %post-stuff regarding it uncomment the following:
# %post devel

## if you want to do something after uninstallation uncomment the following
## and list the actions to perform. But be aware of e.g. deleting directories,
## see the example below how to do it:
# %postun
## if [ "$1" = "0" ]; then
##	rm -rf %{_prefix}/zzz
## fi

## If you want to have a devel-package to be generated and do some
## %postun-stuff regarding it uncomment the following:
# %postun devel

## Its similar for %pre, %preun, %pre devel, %preun devel.

%clean
rm -rf $RPM_BUILD_ROOT

#%pre p3
echo "Bypass Adding user p3"
#useradd -c 'Capfast p3 user' -md %{homep3} -u 2995 -p '$1$kG$4Lx1I5UrZ8DkuDGVV2VtM1' p3

#%postun p3 
#userdel -r -f p3

%files
%defattr(-,root,root)
/%{prefix}/%{gemopt}/capfast/
#/%{prefix}/%{gemopt}/capfast/bin/*
/%{prefix}/etc/profile.d/capfast-env.csh

%files p3
%defattr(-,root,root)
%{homep3}
#%{homep3}/wcs/lib

%changelog
* Mon Jan 06 2014 Matt Rippa <mrippa@gemini.edu> 0.1-2
- Initial release
