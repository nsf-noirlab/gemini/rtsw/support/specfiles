%define _prefix __auto__
%define gemopt opt
%define name epics_module-seq
%define version 2.1.13
%define release 1
%define repository gemdev

%define debug_package %{nil}
%define checkout %(svnversion -cn %{sourcedir} | sed -r 's/.+://') 


Summary: %{name} Package, a module for EPICS base
Name: %{name}
Version: %{version}
#Release: %{release}%{?dist}
Release: %release.%(date +"%Y%m%d")svn%{checkout}%{?dist}
License: EPICS Open License
Group: Applications/Engineering
Source0: %{name}-%{version}.tar.gz
ExclusiveArch: %{arch}
Prefix: %{_prefix}
## You may specify dependencies here
BuildRequires: epics-base-devel
Requires: epics-base
## Switch dependency checking off
# AutoReqProv: no

%description
EPICS is a set of Open Source software tools, libraries and applications developed collaboratively and used worldwide to create distributed soft real-time control systems for scientific instruments such as a particle accelerators, telescopes and other large scientific experiments.
This is the module %{name}.

## If you want to have a devel-package to be generated uncomment the following:
%package devel
Summary: %{name}-devel Package
Group: Development/Gemini
Requires: %{name}
%description devel
EPICS is a set of Open Source software tools, libraries and applications developed collaboratively and used worldwide to create distributed soft real-time control systems for scientific instruments such as a particle accelerators, telescopes and other large scientific experiments.
This is the module %{name}.

## Of course, you also can create additional packages, e.g for "doc". Just
## follow the same way as I did with "%package devel".

%prep
%setup -q -n %{name}

%build
	(cd seq-%{version} && make distclean uninstall && make)

%install
#%define __os_install_post %{nil}
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/seq
cp -r seq-%{version}/dbd $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/seq
cp -r seq-%{version}/bin $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/seq
cp -r seq-%{version}/lib $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/seq
cp -r seq-%{version}/include $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/seq
cp -r seq-%{version}/configure $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/seq/
find $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/seq/configure -name ".svn" -exec rm -rf {} +


%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{gemopt}/epics/modules/seq
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{gemopt}/epics/modules/seq/bin
   /%{_prefix}/%{gemopt}/epics/modules/seq/lib

%files devel
%defattr(-,root,root)
   /%{_prefix}/%{gemopt}/epics/modules/seq/dbd
   /%{_prefix}/%{gemopt}/epics/modules/seq/include
   /%{_prefix}/%{gemopt}/epics/modules/seq/configure

%changelog
 * Fri Mar 10 2012 Mathew Rippa <mrippa@gemini.edu> 2.1.13-0
 - r3.14.12.2, rpmlint compliant
* Wed Feb 18 2008 Felix Kraemer <fkraemer@gemini.edu> 2.0.11-0
- changes to be compliant with EPICS build mechanism
* Wed Dec 19 2007 Felix Kraemer <fkraemer@gemini.edu> 2.0.11-0
- initial release
