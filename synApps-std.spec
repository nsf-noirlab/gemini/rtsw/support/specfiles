%define _prefix __auto__
%define gemopt opt
%define name epics_module-synApps-std
%define version 3.14.12.2
%define release 3.1
%define repository gemdev
%define debug_package %{nil}
%define checkout  %(svnversion -cn %{sourcedir} | sed -r 's/.+://') 

Summary: %{name} Package, Gemini specific records for EPICS base
Name: %{name}
Version: %{version}
#Release: %{release}%{?dist}
Release: %release.%(date +"%Y%m%d")svn%{checkout}%{?dist}
License: GPL
Group: Gemini
BuildRoot: /var/tmp/%{name}-%{version}-root
Source0: %{name}-%{version}.tar.gz
BuildArch: %{arch}
Prefix: %{_prefix}
BuildRequires: epics-base-devel
Requires: epics-base-devel

%description
This is the module %{name}. EPICS SynApps-std module.

## If you want to have a devel-package to be generated uncomment the following:
%package devel
Summary: %{name}-devel Package
Group: Development/Gemini
Requires: %{name}
%description devel
This is the module %{name}-devel. EPICS SynApps-std module.

%prep
%setup -q -n %{name}

%build
make distclean uninstall
make

%install
%define __os_install_post %{nil}
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/synApps-std
cp -r bin $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/synApps-std/
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/synApps-std/
cp -r include $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/synApps-std/
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{gemopt}/epics/modules/synApps-std/

%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{gemopt}/epics/modules/synApps-std
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{gemopt}/epics/modules/synApps-std/bin
   /%{_prefix}/%{gemopt}/epics/modules/synApps-std/lib

%files devel
%defattr(-,root,root)
   /%{_prefix}/%{gemopt}/epics/modules/synApps-std/include
   /%{_prefix}/%{gemopt}/epics/modules/synApps-std/dbd

%changelog
* Thu Mar 14 2013 Matt Rippa <mrippa@gemini.edu> 3.14.12.2-3.1
- gemdev init
